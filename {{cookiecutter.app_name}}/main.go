{% if cookiecutter.open_source_license == 'GNU General Public License v3' %}/*
GNU GENERAL PUBLIC LICENSE
                      Version 3, 29 June 2007

    Copyright (C) {% now 'local', '%Y'%}  {{cookiecutter.maintainer_name}}

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

Also add information on how to contact you by electronic and paper mail.

  You should also get your employer (if you work as a programmer) or school,
if any, to sign a "copyright disclaimer" for the program, if necessary.
For more information on this, and how to apply and follow the GNU GPL, see
<http://www.gnu.org/licenses/>.

  The GNU General Public License does not permit incorporating your program
into proprietary programs.  If your program is a subroutine library, you
may consider it more useful to permit linking proprietary applications with
the library.  If this is what you want to do, use the GNU Lesser General
Public License instead of this License.  But first, please read
<http://www.gnu.org/philosophy/why-not-lgpl.html>.
*/

{% endif %}package main

import (
	"log"
	"strconv"

	"github.com/nanu-c/qml-go"
)

func main() {
	err := qml.Run(run)
	if err != nil {
		log.Fatal(err)
	}
}

func run() error {
	engine := qml.NewEngine()
	component, err := engine.LoadFile("qml/Main.qml")
	if err != nil {
		return err
	}

	testvar := TestStruct{Message: "Hello World", Number: 1}
	context := engine.Context()
	context.SetVar("testvar", &testvar)
	testvar.GetMessage()

	win := component.CreateWindow(nil)
	testvar.Root = win.Root()
	win.Show()
	win.Wait()

	return nil
}

type TestStruct struct {
	Root    qml.Object
	Message string
	Output  string
	Number  int
}

func (testvar *TestStruct) GetMessage() {
	//go func() {
		testvar.Number = testvar.Number + 1
		testvar.Output = "Counter " + strconv.Itoa(testvar.Number)
		qml.Changed(testvar, &testvar.Output)
	//}()
}
